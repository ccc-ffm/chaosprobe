$fn=75;

include <joint.scad>;

/*
   Module: vertebral
   
   Generates a vertebral of the arm. A vertebral consists of a <joint_head> 
   connected directly to a <joint_socket>.
*/
module vertebral() {
	translate([0,0,4]) joint_head();
	translate([0,0,13]) joint_socket();
}

vertebral();
