/*
    Module: joint_hole
    
    The hole in the socket that grabs the joint head. if you connect a 
    <jount_socket> to your component you might need to subtract the joint_hole 
    from your compoenent.
*/
module joint_hole() {
	sphere(d=10.8);									// joint socket hole
}

module joint_socket() {
	difference() {
		sphere(d=14);	// joint socket
		union() {
			joint_hole();
			translate([0,0,-9]) cylinder(12, d1=1, d2=6, center=true);	// spnal canal
			translate([0,0,12]) cube([30,30,20], center=true);				// top cut
			for(i=[0,120,240]) {														// relief cuts
				rotate([0,0,i]) union() {
					translate([0,0,-2]) rotate([0,-90,0]) cylinder(h=30, d=4);
					translate([-10,0,8]) cube([20,3,20], center=true);
				}
			}
		}
	}
}

module joint_head() {
	difference(){
		union() {
			sphere(d=11);		// joint head
		}
		union() {
			translate([0,0,-14]) cube([30,30,20], center=true);			// bottom cut
			translate([0,0,9]) joint_hole();
			cylinder(12, d1=1, d2=6, center=true);	// spnal canal
		}
	}
}
