/*
    Module: round_base
    
    A base plate with predetermined braking points. This is just a base plate 
    that gives your component a firm grip on the table. It is removed after 
    printing process. 
    
    Parameters:
        inner: 
            Inner diameter of the base plate. This should be the diameter of 
            your component at the bottom. There will be a 1mm wide ring around 
            your component left free except for the predetermined braking 
            points.
        outer: 
            Outer diameter of the base plate.
*/
module round_base(inner, outer) {
	height=1;
	space=2;
	translate([0,0,height/2]) {
		difference() {
			cylinder(height, d=outer, center=true);
			cylinder(2*height+1, d=inner+space, center=true);
		}
		difference(){
			union() {
				cube([inner+2,inner/4,height], center=true);
				cube([inner/4,inner+2,height], center=true);
			}
			cylinder(2*height+1, d=inner, center=true);
		}
	}
}

//round_base(3, 20);
