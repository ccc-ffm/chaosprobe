$fn=75;

include <helper.scad>;
include <joint.scad>;

/*
    Module: probe
    
    A probe holder for <PTR H1007C http://www.ptr.eu/de/produkte/federkontakte/standard-federkontakte/serie-1007/>. 
    The base plate is just there for assistance and to be removed after 
    printing.
*/
module probe() {
	difference() {
		union() {
			round_base(6,20);
			cylinder(15, d1=6, d2=8);								// probe container
//			translate([0,0,12]) cylinder(3, d1=4, d2=6.5);	// cable room
			translate([0,0,17]) sphere(d=10);						// cable room
			translate([0,0,22]) joint_socket();
		}
		union() {
			translate([0,0,-0.5]) cylinder(20, d=2);
			translate([0,0,11.5]) cylinder(4, d1=2, d2=4.4);	// cable room
			translate([0,0,17]) sphere(d=6);						// cable room
			translate([0,0,22]) joint_hole();
		}
	}
}

/*difference(){
	probe();
	cube([20,20,30]);
} // */

probe();
