$fn=100;
include <roundCornersCube.scad>
x = 180;
y = 180;
z=6;

module screw_hole(x,y,r_head,h_head,r_hole) {
	translate([x,y,0]) cylinder(r=r_head,h=h_head);
	translate([x,y,0]) cylinder(h=z+1,r=r_hole,,center=true);
}

difference() {
	roundCornersCube(x,y,z,5);
	cylinder(h=60, r=5,center=true);


hull() {
   translate([75,0,-10]) cylinder(h=60, r=3);
	translate([12,0,-10]) cylinder(h=60, r=3);
 }

hull() {
   translate([-75,0,-10]) cylinder(h=60, r=3);
	translate([-12,0,-10]) cylinder(h=60, r=3);
 }

hull() {
   translate([0,75,-10]) cylinder(h=60, r=3);
	translate([0,12,-10]) cylinder(h=60, r=3);
 }

hull() {
   translate([0,-75,-10]) cylinder(h=60, r=3);
	translate([0,-12,-10]) cylinder(h=60, r=3);
 }

for (z = [0:18:360])
{
    translate([77*sin(z), 77*cos(z), -10]) cylinder (h = 60, r=3);
}

translate([30,30,-10]) roundCornersCube(40,40,60,5);
translate([-30,30,-10]) roundCornersCube(40,40,60,5);
translate([30,-30,-10]) roundCornersCube(40,40,60,5);
translate([-30,-30,-10]) roundCornersCube(40,40,60,5);

screw_hole(75,75,4,5,2);
screw_hole(-75,75,4,5,2);
screw_hole(75,-75,4,5,2);
screw_hole(-75,-75,4,5,2);

}